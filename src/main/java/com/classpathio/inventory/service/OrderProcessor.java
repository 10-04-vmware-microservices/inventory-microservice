package com.classpathio.inventory.service;

import java.util.function.Consumer;

import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

@Service
public class OrderProcessor {
	
	@Bean
	public Consumer<Message<String>> consumer() {
	    return message -> System.out.println("process the order " + message);
	}


}
